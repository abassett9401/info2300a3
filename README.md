# Trading Post

Aaron Bassett PROG 2300 Assignmnet 3 

## Requirements

- Dotnet core 2.1
- mysql
- node 8

## Configuration

Edit `src/TradingPost/appsettings.json` connection string to match your database.
The default string will connect to the default localdb install on windows;

```JSON
"ConnectionStrings": {
   "TradingPost": "Server=(localdb)\\mssqllocaldb;Database=TradingPost Trusted_Connection=True;"
},
```

## Running

### Cli

Update and run project
```bash
dotnet restore
dotnet ef database update
dotnet run
```

### Visual Studio

Use the easy check mark


