using System.Threading.Tasks;
using Moq;
using TradingPost.Features.ListingComment;
using TradingPost.Infrastructure.Security;
using TradingPost.Infrastructure.Errors;
using Xunit;
using System.Net;
using System;
using TradingPost.Infrastructure;

namespace TradingPost.UnitTests.Features.ListingComment
{
  public class UpdateTests : SliceFixture
  {
    [Fact]
    public async Task Expect_Update_ListingComment()
    {
        var command = new Update.Command()
        {
            ListingComment = new Update.ListingCommentData
            {
                id = 1,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment Updated",
                commentBody = "Body of a test listing comment Updated."
            }
        };

        var listing = new Models.Listing()
        {
            id = 1,
            UserId = 2,
            ItemId = 1,
            datePosted = DateTime.Now,
            listingTitle = "Test Listing",
            listingBody = "Body of a test listing."
        };

        var listingComment = new Models.ListingComment()
        {
            id = 1,
            UserId = 1,
            ListingId = 1,
            datePosted = DateTime.Now,
            commentTitle = "Test Listing Comment",
            commentBody = "Body of a test listing comment."
        };

        await InsertAsync(listing);
        await InsertAsync(listingComment);

        var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 1});      

        var handler = new Update.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

        var result = await handler.Handle(command, new System.Threading.CancellationToken());

        Assert.NotNull(result);
        Assert.Equal(result.id, command.ListingComment.id);
        Assert.Equal(result.datePosted, command.ListingComment.datePosted);
        Assert.Equal(result.commentTitle, command.ListingComment.commentTitle);
        Assert.Equal(result.commentBody, command.ListingComment.commentBody);
    }

    [Fact]
    public async Task Expect_RestException_InvalidUser()
    {
        var command = new Update.Command()
        {
            ListingComment = new Update.ListingCommentData
            {
                id = 1,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment Updated",
                commentBody = "Body of a test listing comment Updated."
            }
        };

        var listing = new Models.Listing()
        {
            id = 1,
            UserId = 2,
            ItemId = 1,
            datePosted = DateTime.Now,
            listingTitle = "Test Listing",
            listingBody = "Body of a test listing."
        };

        var listingComment = new Models.ListingComment()
        {
            id = 1,
            UserId = 1,
            ListingId = 1,
            datePosted = DateTime.Now,
            commentTitle = "Test Listing Comment",
            commentBody = "Body of a test listing comment."
        };

        await InsertAsync(listing);
        await InsertAsync(listingComment);

        var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 2});      

        var handler = new Update.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

        var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        Assert.Equal(result.Code, HttpStatusCode.BadRequest);
    }

    [Fact]
    public async Task Expect_RestException_InvalidListingComment()
    {
       var command = new Update.Command()
        {
            ListingComment = new Update.ListingCommentData
            {
                id = 2,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment Updated",
                commentBody = "Body of a test listing comment Updated."
            }
        };

        var listing = new Models.Listing()
        {
            id = 1,
            UserId = 2,
            ItemId = 1,
            datePosted = DateTime.Now,
            listingTitle = "Test Listing",
            listingBody = "Body of a test listing."
        };

        var listingComment = new Models.ListingComment()
        {
            id = 1,
            UserId = 1,
            ListingId = 1,
            datePosted = DateTime.Now,
            commentTitle = "Test Listing Comment",
            commentBody = "Body of a test listing comment."
        };

        await InsertAsync(listing);
        await InsertAsync(listingComment);

        var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 1});      

        var handler = new Update.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

        var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        Assert.Equal(result.Code, HttpStatusCode.BadRequest);
    }
  }
}