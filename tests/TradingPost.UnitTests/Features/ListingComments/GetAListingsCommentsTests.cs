using System;
using System.Net;
using System.Threading.Tasks;
using TradingPost.Features.ListingComment;
using TradingPost.Infrastructure.Errors;
using Xunit;

namespace TradingPost.UnitTests.Features.ListingComment
{
    public class GetAListingsCommentsTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Get_ListingComments()
        {
            var command = new GetAListingsComments.Query()
            {
                listingId = 1
            };

            var listing = new Models.Listing()
            {
                id = 1,
                UserId = 2,
                ItemId = 1,
                datePosted = DateTime.Now,
                listingTitle = "Test Listing",
                listingBody = "Body of a test listing."
            };

            var listingComment = new Models.ListingComment()
            {
                id = 1,
                ListingId = 1,
                UserId = 1,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment",
                commentBody = "Body of a test listing comment."
            };

            var listingComment2 = new Models.ListingComment()
            {
                id = 2,
                ListingId = 1,
                UserId = 12,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment 2",
                commentBody = "Body of a test listing comment 2."
            };

            var listingComment3 = new Models.ListingComment()
            {
                id = 3,
                ListingId = 1,
                UserId = 2,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment 3",
                commentBody = "Body of a test listing comment 3."
            };

            await InsertAsync(listing);
            await InsertAsync(listingComment);
            await InsertAsync(listingComment2);
            await InsertAsync(listingComment3);

            var handler = new GetAListingsComments.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await handler.Handle(command, new System.Threading.CancellationToken());

            Assert.Equal(result.Count, 3);
        }

        [Fact]
        public async Task Expect_RestException_NotFound()
        {
            var command = new Get.Query()
            {
                id = 1
            };

            var listing = new Models.Listing()
            {
                id = 2,
                UserId = 2,
                ItemId = 1,
                datePosted = DateTime.Now,
                listingTitle = "Test Listing",
                listingBody = "Body of a test listing."
            };

            await InsertAsync(listing);

            var handler = new Get.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
            
            Assert.Equal(result.Code, HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Expect_Zero_Listings()
        {
            var command = new GetAListingsComments.Query()
            {
                listingId = 1
            };

            var listing = new Models.Listing()
            {
                id = 1,
                UserId = 2,
                ItemId = 1,
                datePosted = DateTime.Now,
                listingTitle = "Test Listing",
                listingBody = "Body of a test listing."
            };

            await InsertAsync(listing);

            var handler = new GetAListingsComments.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await handler.Handle(command, new System.Threading.CancellationToken());

            Assert.Equal(result.Count, 0);
        }
    }
}