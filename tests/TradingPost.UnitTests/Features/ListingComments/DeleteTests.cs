using System;
using System.Net;
using System.Threading.Tasks;
using TradingPost.Features.ListingComment;
using TradingPost.Infrastructure.Errors;
using Xunit;

namespace TradingPost.UnitTests.Features.ListingComment
{
    public class DeleteTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Delete_ListingComment()
        {
            var command = new Delete.Query()
            {
                id = 1,
            };

            var listingComment = new Models.ListingComment()
            {
                id = 1,
                UserId = 1,
                ListingId = 1,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment",
                commentBody = "Body of a test listing comment."
            };

            await InsertAsync(listingComment);

            var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 1}); 

            var handler = new Delete.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

            var result = await handler.Handle(command, new System.Threading.CancellationToken());
                 
        }

        [Fact]
        public async Task Expect_RestException_NotFound()
        {
            var command = new Delete.Query()
            {
                id = 1,
            };

            var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 1}); 

            var handler = new Delete.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

            var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
            
            Assert.Equal(result.Code, HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Expect_RestException_InvalidUser()
        {
            var command = new Delete.Query()
            {
                id = 1,
            };

            var listingComment = new Models.ListingComment()
            {
                id = 1,
                UserId = 1,
                ListingId = 1,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment",
                commentBody = "Body of a test listing comment."
            };

            await InsertAsync(listingComment);

            var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){}); 

            var handler = new Delete.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

            var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
            
            Assert.Equal(result.Code, HttpStatusCode.BadRequest);
        }
    }
}