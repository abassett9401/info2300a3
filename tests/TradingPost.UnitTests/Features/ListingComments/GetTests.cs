using System;
using System.Net;
using System.Threading.Tasks;
using TradingPost.Features.ListingComment;
using TradingPost.Infrastructure.Errors;
using Xunit;

namespace TradingPost.UnitTests.Features.ListingComment
{
    public class GetTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Get_ListingComment()
        {
            var command = new Get.Query()
            {
                id = 1
            };

            var listingComment = new Models.ListingComment()
            {
                id = 1,
                ListingId = 1,
                UserId = 1,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment",
                commentBody = "Body of a test listing comment."
            };

            await InsertAsync(listingComment);

            var handler = new Get.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await handler.Handle(command, new System.Threading.CancellationToken());

            Assert.Equal(result.id, command.id);
            Assert.Equal(result.userId, 1);
            Assert.Equal(result.listingId, 1);
            Assert.Equal(result.datePosted, listingComment.datePosted);
            Assert.Equal(result.commentTitle, "Test Listing Comment");
            Assert.Equal(result.commentBody, "Body of a test listing comment.");
        }

        [Fact]
        public async Task Expect_RestException_NotFound()
        {
            var command = new Get.Query()
            {
                id = 2
            };

            var listingComment = new Models.ListingComment()
            {
                id = 1,
                ListingId = 1,
                UserId = 1,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment",
                commentBody = "Body of a test listing comment."
            };

            await InsertAsync(listingComment);

            var handler = new Get.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
            
            Assert.Equal(result.Code, HttpStatusCode.BadRequest);
        }
    }
}