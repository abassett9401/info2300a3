using System.Threading.Tasks;
using Moq;
using TradingPost.Features.ListingComment;
using TradingPost.Infrastructure.Security;
using TradingPost.Infrastructure.Errors;
using Xunit;
using System.Net;
using System;
using TradingPost.Infrastructure;

namespace TradingPost.UnitTests.Features.ListingComment
{
  public class CreateTests : SliceFixture
  {
    [Fact]
    public async Task Expect_Create_ListingComment()
    {
        var command = new Create.Command()
        {
            ListingComment = new Create.ListingCommentData
            {
                listingId = 1,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment",
                commentBody = "Body of a test listing comment."
            }
        };

        var listing = new Models.Listing()
        {
            id = 1,
            UserId = 1,
            ItemId = 1,
            datePosted = DateTime.Now,
            listingTitle = "Test Listing",
            listingBody = "Body of a test listing."
        };

        await InsertAsync(listing);

        var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 1});      

        var handler = new Create.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

        var result = await handler.Handle(command, new System.Threading.CancellationToken());

        Assert.NotNull(result);
        Assert.Equal(result.listingId, command.ListingComment.listingId);
        Assert.Equal(result.datePosted, command.ListingComment.datePosted);
        Assert.Equal(result.commentTitle, command.ListingComment.commentTitle);
        Assert.Equal(result.commentBody, command.ListingComment.commentBody);
    }

    [Fact]
    public async Task Expect_RestException_InvalidUser()
    {
        var command = new Create.Command()
        {
            ListingComment = new Create.ListingCommentData
            {
                listingId = 1,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment",
                commentBody = "Body of a test listing comment."
            }
        };

        var listing = new Models.Listing()
        {
            id = 1,
            UserId = 1,
            ItemId = 1,
            datePosted = DateTime.Now,
            listingTitle = "Test Listing",
            listingBody = "Body of a test listing."
        };

        await InsertAsync(listing);

        var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){});      

        var handler = new Create.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

        var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        Assert.Equal(result.Code, HttpStatusCode.BadRequest);
    }

    [Fact]
    public async Task Expect_RestException_Listing_NotFound()
    {
        var command = new Create.Command()
        {
            ListingComment = new Create.ListingCommentData
            {
                listingId = 2,
                datePosted = DateTime.Now,
                commentTitle = "Test Listing Comment",
                commentBody = "Body of a test listing comment."
            }
        };

        var listing = new Models.Listing()
        {
            id = 1,
            UserId = 1,
            ItemId = 1,
            datePosted = DateTime.Now,
            listingTitle = "Test Listing",
            listingBody = "Body of a test listing."
        };

        await InsertAsync(listing);

        var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 1});      

        var handler = new Create.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

        var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        Assert.Equal(result.Code, HttpStatusCode.BadRequest);
    }
  }
}