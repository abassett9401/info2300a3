using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TradingPost.Features.User;
using FluentAssertions;
using Xunit;

namespace TradingPost.UnitTests.Features.User
{
    public class GetTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Get_User()
        {


            var expected = new Models.User()
            {
                email = "email",
                userName = "username",
                hash = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 },
                salt = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 }
            };

            var db = GetDbContext();
            db.Users.Add(expected);
            db.SaveChanges();

            var query = new Get.Query()
            {
                id = expected.id
            };

            var handler = new Get.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            result.Should().NotBeNull();
            result.email.Should().Equals(expected.email);
            result.userName.Should().Equals(expected.userName);
        }
    }
}