using System.Threading.Tasks;
using Moq;
using TradingPost.Features.User;
using TradingPost.Infrastructure.Security;
using TradingPost.Infrastructure.Errors;
using Xunit;
using System.Net;

namespace TradingPost.UnitTests.Features.User
{
  public class CreateTests : SliceFixture
  {
    [Fact]
    public async Task Expect_Create_User()
    {

        var hashData = new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };
        var command = new Create.Command()
        {
            User = new Create.UserData
            {
                Email = "Email",
                Password = "pass",
                Username = "user"
            }
        };
        var mockPasswordHasher = new Mock<IPasswordHasher>();
        mockPasswordHasher.Setup((ph) => ph.Hash(It.IsAny<string>())).Returns(new HashResult()
        {
            hash = hashData,
            salt = hashData
        });
        var handler = new Create.Handler(GetDbContext(), CreateMapper<MappingProfile>(), mockPasswordHasher.Object);

        var result = await handler.Handle(command, new System.Threading.CancellationToken());

        Assert.NotNull(result);
        Assert.Equal(command.User.Email ,result.email);
        Assert.Equal(command.User.Username,result.userName);

        mockPasswordHasher.Verify(ph => ph.Hash(command.User.Password));
    }

    [Fact]
    public async Task Expect_RestException_Email_In_Use() {
        var command = new Create.Command()
        {
            User = new Create.UserData
            {
                Email = "Email",
                Password = "pass",
                Username = "user"
            }
        };
        await InsertAsync(new Models.User(){
                email="Email",
                hash= new byte[] { 0x20 },
                userName="newUser",
                salt = new byte[] { 0x20 }
            });

        var mockPasswordHasher = new Mock<IPasswordHasher>();
        var handler = new Create.Handler(GetDbContext(), CreateMapper<MappingProfile>(), mockPasswordHasher.Object);

        var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        Assert.Equal(HttpStatusCode.BadRequest,result.Code);
    }

    [Fact]
    public async Task Expect_RestException_UserName_In_Use() {
        var command = new Create.Command()
        {
            User = new Create.UserData
            {
                Email = "Email",
                Password = "pass",
                Username = "user"
            }
        };
        await InsertAsync(new Models.User(){
                email="Email2",
                hash= new byte[] { 0x20 },
                userName="user",
                salt = new byte[] { 0x20 }
            });

        var mockPasswordHasher = new Mock<IPasswordHasher>();
        var handler = new Create.Handler(GetDbContext(), CreateMapper<MappingProfile>(), mockPasswordHasher.Object);

        var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        Assert.Equal(HttpStatusCode.BadRequest,result.Code);
    }
  }
}