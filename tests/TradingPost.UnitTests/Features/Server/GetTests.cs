using System.Threading.Tasks;
using TradingPost.Features.Server;
using Xunit;

namespace TradingPost.UnitTests.Features.Server
{
    public class GetTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Get_Server(){
            var query = new Get.Query()
            {
                id = 1
            };

            var expected = new Models.Server()
            {
                id = 1,
                serverName = "testServer",
                RegionId = 1
            };

            var db = GetDbContext();
            db.Servers.Add(expected);
            db.SaveChanges();

            var handler = new Get.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.Equal(result.id, expected.id);
            Assert.Equal(result.serverName, expected.serverName);
            Assert.Equal(result.RegionId, expected.RegionId);
        }
    }
}