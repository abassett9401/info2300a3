using System.Threading.Tasks;
using TradingPost.Features.Region;
using Xunit;

namespace TradingPost.UnitTests.Features.Region
{
    public class GetTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Get_Region(){
            var query = new Get.Query()
            {
                id = 1
            };

            var expected = new Models.Region()
            {
                id = 1,
                region = "testregion"
            };

            var db = GetDbContext();
            db.Regions.Add(expected);
            db.SaveChanges();

            var handler = new Get.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.Equal(result.id, expected.id);
            Assert.Equal(result.region, expected.region);
        }
    }
}