using System.Threading.Tasks;
using TradingPost.Features.Game;
using Xunit;

namespace TradingPost.UnitTests.Features.Game
{
    public class GetTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Get_Game(){
            
            var expected = new Models.Game()
            {
                id = 100,
                gameTitle = "testgame"
            };
            
            var db = GetDbContext();
            db.Games.Add(expected);
            db.SaveChanges();
            var query = new Get.Query()
            {
                id = expected.id
            };

            var handler = new Get.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.Equal(result.id, expected.id);
            Assert.Equal(result.gameTitle, expected.gameTitle);
        }
    }
}