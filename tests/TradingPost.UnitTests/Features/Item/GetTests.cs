using System.Threading.Tasks;
using TradingPost.Features.Item;
using Xunit;

namespace TradingPost.UnitTests.Features.Item
{
    public class GetTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Get_Item(){
            var query = new Get.Query()
            {
                id = 100
            };

            var expected = new Models.Item()
            {
                id = 100,
                itemName = "testgame",
                GameID = 1,
                quality = 1,
                description = "testDescription"
            };

            var db = GetDbContext();
            db.Items.Add(expected);
            db.SaveChanges();

            var handler = new Get.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            Assert.NotNull(result);
            Assert.Equal(result.id, expected.id);
            Assert.Equal(result.itemName, expected.itemName);
            Assert.Equal(result.GameID, expected.GameID);
            Assert.Equal(result.quality, expected.quality);
            Assert.Equal(result.description, expected.description);
        }
    }
}