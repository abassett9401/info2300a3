using System;
using System.Net;
using System.Threading.Tasks;
using TradingPost.Features.Listing;
using TradingPost.Infrastructure.Errors;
using Xunit;

namespace TradingPost.UnitTests.Features.Listing
{
    public class GetTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Get_Listing()
        {
            var command = new Get.Query()
            {
                id = 1
            };

            var listing = new Models.Listing()
            {
                id = 1,
                UserId = 1,
                ItemId = 1,
                datePosted = DateTime.Now,
                listingTitle = "Test Listing",
                listingBody = "Body of a test listing."
            };

            await InsertAsync(listing);

            var handler = new Get.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await handler.Handle(command, new System.Threading.CancellationToken());

            Assert.Equal(result.id, command.id);
            Assert.Equal(result.userId, 1);
            Assert.Equal(result.itemId, 1);
            Assert.Equal(result.datePosted, listing.datePosted);
            Assert.Equal(result.listingTitle, "Test Listing");
            Assert.Equal(result.listingBody, "Body of a test listing.");
        }

        [Fact]
        public async Task Expect_RestException_NotFound()
        {
            var command = new Get.Query()
            {
                id = 2
            };

            var listing = new Models.Listing()
            {
                id = 1,
                UserId = 1,
                ItemId = 1,
                datePosted = DateTime.Now,
                listingTitle = "Test Listing",
                listingBody = "Body of a test listing."
            };

            await InsertAsync(listing);

            var handler = new Get.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
            
            Assert.Equal(result.Code, HttpStatusCode.BadRequest);
        }
    }
}