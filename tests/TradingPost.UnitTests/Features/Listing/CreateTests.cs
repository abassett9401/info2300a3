using System.Threading.Tasks;
using Moq;
using TradingPost.Features.Listing;
using TradingPost.Infrastructure.Security;
using TradingPost.Infrastructure.Errors;
using Xunit;
using System.Net;
using System;
using TradingPost.Infrastructure;

namespace TradingPost.UnitTests.Features.Listing
{
  public class CreateTests : SliceFixture
  {
    [Fact]
    public async Task Expect_Create_Listing()
    {
        var command = new Create.Command()
        {

                itemId = 1,
                listingTitle = "Test Listing",
                listingBody = "Body of a test listing."
            
        };

        var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 1});      

        var handler = new Create.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

        var result = await handler.Handle(command, new System.Threading.CancellationToken());

        Assert.NotNull(result);
        Assert.Equal(result.itemId, 1);
        Assert.Equal(result.listingTitle, "Test Listing");
        Assert.Equal(result.listingBody, "Body of a test listing.");
    }

    [Fact]
    public async Task Expect_RestException_InvalidUser()
    {
        var command = new Create.Command()
        {

                itemId = 1,
                listingTitle = "Test Listing",
                listingBody = "Body of a test listing."
            
        };

        var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){});      

        var handler = new Create.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

        var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        Assert.Equal(result.Code, HttpStatusCode.BadRequest);
    }
  }
}