using System;
using System.Net;
using System.Threading.Tasks;
using TradingPost.Features.Listing;
using TradingPost.Infrastructure.Errors;
using Xunit;

namespace TradingPost.UnitTests.Features.Listing
{
    public class DeleteTests : SliceFixture
    {
        [Fact]
        public async Task Expect_Delete_Listing()
        {
            var command = new Delete.Query()
            {
                id = 1,
            };

            var listing = new Models.Listing()
            {
                id = 1,
                UserId = 1,
                ItemId = 1,
                datePosted = DateTime.Now,
                listingTitle = "Test Listing",
                listingBody = "Body of a test listing."
            };

            await InsertAsync(listing);

            var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 1}); 

            var handler = new Delete.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

            var result = await handler.Handle(command, new System.Threading.CancellationToken());
                 
        }

        [Fact]
        public async Task Expect_RestException_NotFound()
        {
            var command = new Delete.Query()
            {
                id = 1,
            };

            var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 1}); 

            var handler = new Delete.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

            var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
            
            Assert.Equal(result.Code, HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Expect_RestException_InvalidUser()
        {
            var command = new Delete.Query()
            {
                id = 1,
            };

            var listing = new Models.Listing()
            {
                id = 1,
                UserId = 1,
                ItemId = 1,
                datePosted = DateTime.Now,
                listingTitle = "Test Listing",
                listingBody = "Body of a test listing."
            };

            await InsertAsync(listing);

            var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){}); 

            var handler = new Delete.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

            var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
            
            Assert.Equal(result.Code, HttpStatusCode.BadRequest);
        }
    }
}