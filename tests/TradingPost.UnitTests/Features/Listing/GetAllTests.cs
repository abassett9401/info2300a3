using System;
using System.Net;
using System.Threading.Tasks;
using TradingPost.Features.Listing;
using TradingPost.Infrastructure.Errors;
using Xunit;

namespace TradingPost.UnitTests.Features.Listing
{
    public class GetAllTests : SliceFixture
    {
        [Fact]
        public async Task Expect_GetAll_Listings()
        {
            var command = new GetAll.Query()
            {
            };

            var listing = new Models.Listing()
            {
                id = 1,
                UserId = 1,
                ItemId = 1,
                datePosted = DateTime.Now,
                listingTitle = "Test Listing",
                listingBody = "Body of a test listing."
            };

            var listing2 = new Models.Listing()
            {
                id = 2,
                UserId = 3,
                ItemId = 14,
                datePosted = DateTime.Now,
                listingTitle = "Test Listing 2",
                listingBody = "Body of a test listing 2."
            };

            var listing3 = new Models.Listing()
            {
                id = 3,
                UserId = 5,
                ItemId = 4,
                datePosted = DateTime.Now,
                listingTitle = "Test Listing 3",
                listingBody = "Body of a test listing 3."
            };

            await InsertAsync(listing);
            await InsertAsync(listing2);
            await InsertAsync(listing3);

            var handler = new GetAll.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await handler.Handle(command, new System.Threading.CancellationToken());

            Assert.Equal(result.Count, 3);
        }

        [Fact]
        public async Task Expect_Zero_Listings()
        {
            var command = new GetAll.Query()
            {
            };

            var handler = new GetAll.Handler(GetDbContext(), CreateMapper<MappingProfile>());

            var result = await handler.Handle(command, new System.Threading.CancellationToken());

            Assert.Equal(result.Count, 0);
        }
    }
}