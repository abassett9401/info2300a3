using System.Threading.Tasks;
using Moq;
using TradingPost.Features.Listing;
using TradingPost.Infrastructure.Security;
using TradingPost.Infrastructure.Errors;
using Xunit;
using System.Net;
using System;
using TradingPost.Infrastructure;

namespace TradingPost.UnitTests.Features.Listing
{
  public class UpdateTests : SliceFixture
  {
    [Fact]
    public async Task Expect_Update_Listing()
    {
        var command = new Update.Command()
        {
            Listing = new Update.ListingData
            {
                listingId = 1,                
                itemId = 3,
                listingTitle = "Test Listing Updated",
                listingBody = "Body of a test listing Updated."
            }
        };

        var listing = new Models.Listing()
        {
            id = 1,
            UserId = 2,
            ItemId = 1,
            datePosted = DateTime.Now,
            listingTitle = "Test Listing",
            listingBody = "Body of a test listing."
        };

        await InsertAsync(listing);

        var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 2});      

        var handler = new Update.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

        var result = await handler.Handle(command, new System.Threading.CancellationToken());

        Assert.NotNull(result);
        Assert.Equal(result.itemId, command.Listing.itemId);
        Assert.Equal(result.listingTitle, command.Listing.listingTitle);
        Assert.Equal(result.listingBody, command.Listing.listingBody);
    }

    [Fact]
    public async Task Expect_RestException_InvalidUser()
    {
       var command = new Update.Command()
        {
            Listing = new Update.ListingData
            {
                listingId = 1,                
                itemId = 3,
                listingTitle = "Test Listing Updated",
                listingBody = "Body of a test listing Updated."
            }
        };

        var listing = new Models.Listing()
        {
            id = 1,
            UserId = 2,
            ItemId = 1,
            datePosted = DateTime.Now,
            listingTitle = "Test Listing",
            listingBody = "Body of a test listing."
        };

        await InsertAsync(listing);

        var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 1});      

        var handler = new Update.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

        var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        Assert.Equal(result.Code, HttpStatusCode.BadRequest);
    }

    [Fact]
    public async Task Expect_RestException_InvalidListing()
    {
       var command = new Update.Command()
        {
            Listing = new Update.ListingData
            {
                listingId = 2,                
                itemId = 3,
                listingTitle = "Test Listing Updated",
                listingBody = "Body of a test listing Updated."
            }
        };

        var listing = new Models.Listing()
        {
            id = 1,
            UserId = 2,
            ItemId = 1,
            datePosted = DateTime.Now,
            listingTitle = "Test Listing",
            listingBody = "Body of a test listing."
        };

        await InsertAsync(listing);

        var currentUserAccessor = new StubCurrentUserAccessor(new Models.User(){id = 2});      

        var handler = new Update.Handler(GetDbContext(), CreateMapper<MappingProfile>(), currentUserAccessor);

        var result = await Assert.ThrowsAsync<RestException>(() => handler.Handle(command, new System.Threading.CancellationToken()));
        Assert.Equal(result.Code, HttpStatusCode.BadRequest);
    }
  }
}