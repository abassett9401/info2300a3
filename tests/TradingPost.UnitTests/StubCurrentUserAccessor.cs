using System.Threading.Tasks;
using TradingPost.Features.User;
using TradingPost.Infrastructure;

namespace TradingPost.UnitTests
{
    public class StubCurrentUserAccessor : ICurrentUserAccessor
    {
        private readonly Models.User _currentUser;  

        public StubCurrentUserAccessor(Models.User user)
        {
            _currentUser = user;
        }

        public string GetCurrentUsername()
        {
            return _currentUser.userName;
        }

        public int GetId()
        {
            return _currentUser.id;
        }

        public async Task<UserDto> GetUser()
        {
            return new UserDto(){
                email =_currentUser.email,
                id = _currentUser.id,
                userName = _currentUser.userName
            };
        }

        public string GetUsername()
        {
           return _currentUser.userName;
        }

        public bool isAuthentiated()
        {
            return false;
        }
    }
}