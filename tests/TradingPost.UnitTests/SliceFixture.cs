using System;
using System.IO;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TradingPost.Models;
using AutoMapper;

namespace TradingPost.UnitTests
{
    public class SliceFixture : IDisposable
    {
        static readonly IConfiguration Config;
        private readonly TradingPostContext _dbContext;
        private readonly string DbName = Guid.NewGuid() + ".db";


        static SliceFixture()
        {
            Config = new ConfigurationBuilder()
               .AddEnvironmentVariables()
               .Build();
        }

        public SliceFixture()
        {

            DbContextOptionsBuilder<TradingPostContext> builder = new DbContextOptionsBuilder<TradingPostContext>();
            builder.UseInMemoryDatabase(DbName);
            _dbContext = new TradingPostContext(builder.Options);


            GetDbContext().Database.EnsureCreated();
        }

        public TradingPostContext GetDbContext()
        {
            return _dbContext;
        }

        public IMapper CreateMapper<TProfile> () where TProfile : Profile, new(){
            var cfg = new AutoMapper.Configuration.MapperConfigurationExpression();
            cfg.AddProfile<TProfile>();
            return  new Mapper(new MapperConfiguration(cfg));
        }

        public void Dispose()
        {
            File.Delete(DbName);
        }

        public Task ExecuteDbContextAsync(Func<TradingPostContext, Task> action)
        {
            return action(_dbContext);
        }

        public Task<T> ExecuteDbContextAsync<T>(Func<TradingPostContext, Task<T>> action)
        {
            return action(_dbContext);
        }

        public Task InsertAsync(params object[] entities)
        {
            return ExecuteDbContextAsync(db =>
            {
                foreach (var entity in entities)
                {
                    db.Add(entity);
                }
                return db.SaveChangesAsync();
            });
        }
    }
}