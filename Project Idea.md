# Trading Post

Out of band Game Item Market Place where users can list in game items and set up trades with other users.
Users will not use real money with our service (all trades are made in game using in game currency)

Kijiji For Games

## Features
Post buy/sell orders for items in games

Each listing will include:
- Server
- Item (and # of items)
- Price
- Region
	
Compatible with multiple games 
- Link Game to account ex WOW Armory
- Region
- Server
	
Privite posts between buyer and seller
- private messaging/comment system

User Rating
- User comments/reviews/stars

Account statistics 
- Users can see own full order history
- Other users can see public info (How many buy and sell orders (confirmed, scam, canceled))
- Frequent offenders could be flagged (scams, stealing, etc.)

Sorting and filtering orders by:
- Price (hign low)
- listing date (recent posts)
- Game
- Item

Notice/News
- Users report items show noticed of potental scam
- Same with users
- Can also update users on Trading Post updates/info
	
Dashboard of listings
- If multiple
	- Shows games available 
		- Amount of orders 
		- Hot items 
		- Trends

Watch items (wish list)
- Flag items to be updated on any changes in the listing(s)