﻿import React from 'react';
import { Route, Switch } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import Login from './components/Login';
import Register from './components/Register';
import Posts from './components/Posts';
import EditPost from './components/EditPost';
import CreatePost from './components/CreatePost';
import ViewPost from './components/ViewPost';

export default () => {
  return (<Layout>
    <Route exact path='/' component={Home} />
    <Switch>
      <Route exact path='/listings' component={Posts} />
      <Route path='/listings/create' component={CreatePost} />
      <Route path='/listings/:id/edit' component={EditPost} />
      <Route path='/listings/:id' component={ViewPost} />
    </Switch>
    <Route path='/login' component={Login} />
    <Route path='/register' component={Register} />
  </Layout>
);
  }
