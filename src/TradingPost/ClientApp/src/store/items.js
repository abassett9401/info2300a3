﻿const requestGamesType = 'REQUEST_GAMES';
const receiveGamesType = 'RECEIVE_GAMES';
const initialState = { list: [], isLoading: false };

export const actionCreators = {
  list: () => async (dispatch, getState) => {    
    dispatch({ type: requestGamesType });

    const url = `/api/Item`;
    const response = await fetch(url);
    const res = await response.json();

    dispatch({ type: receiveGamesType, items: res });
  }
};

export const reducer = (state = initialState, action) => {

  if (action.type === requestGamesType) {
    return {
      ...state,
      isLoading: true
    };
  }

  if (action.type === receiveGamesType) {
    return {
      ...state,
      list: action.items,
      isLoading: false
    };
  }

  return state;
};
