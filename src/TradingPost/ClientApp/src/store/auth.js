﻿import {push} from 'connected-react-router'

const requestLoginType = 'REQUEST_LOGIN';
const receiveLoginType = 'RECEIVE_LOGIN';
const requestLogoutType = 'REQUEST_LOGOUT';
const initialState = { token: null, user: null, isLoading: false };

const getUser = async (token) => {
  const response = await fetch('user', {
    headers: {
      'Authorization': `Bearer ${token}`
    }
  });
  return await response.json();
} 

export const actionCreators = {
  login: (userName, password) => async (dispatch, getState) => {    
    dispatch({ type: requestLoginType });

    const url = `Users/login`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: userName,password
      })
    });
    const {token} = await response.json();

    const user = await getUser(token)

    dispatch({ type: receiveLoginType, token, user });
    
    dispatch(push('/'));
  },
  logout: () => async (dispatch, getState) => {
    dispatch({ type: requestLogoutType });
    dispatch(push('/'));
  },
  register: (userName,email, password) => async (dispatch, getState) => {    
    dispatch({ type: requestLoginType });

    const url = `Users`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        user:{
          username: userName,password, email
        }
      })
    });
    const res = await response.json();

    dispatch(push('/login'));
  },
};

export const reducer = (state = initialState, action) => {

  if (action.type === requestLoginType) {
    return {
      ...state,
      isLoading: true
    };
  }

  if (action.type === receiveLoginType) {
    return {
      ...state,
      token: action.token,
      user: action.user,
      isLoading: false
    };
  }

  if(action.type === requestLogoutType) {
    return {
      ...state,
      token: null,
      user: null,
    }
  }

  return state;
};
