﻿import {push} from 'connected-react-router'

const requestListingsType = 'REQUEST_LISTINGS';
const receiveListingsType = 'RECEIVE_LISTINGS';
const requestDeleteListingsType = 'REQUEST_LISTING_DELETE';
const receiveDeleteListingsType = 'RECEIVE_LISTING_DELETE';
const requestUpdateListingsType = 'REQUEST_LISTING_UPDATE';
const receiveUpdateListingsType = 'RECEIVE_LISTING_UPDATE';
const requestCreateListingsType = 'REQUEST_LISTING_CREATE';
const receiveCreateListingsType = 'RECEIVE_LISTING_CREATE';

const initialState = { list: [], isLoading: false };

export const actionCreators = {
  list: () => async (dispatch, getState) => {    
    dispatch({ type: requestListingsType });

    const url = `/api/Listings`;
    const response = await fetch(url);
    const res = await response.json();

    dispatch({ type: receiveListingsType, listings: res });
  },
  delete: (id) => async (dispatch, getState) => {
    const token = getState().auth.token;
    dispatch({ type: requestDeleteListingsType });
    const url = `/api/Listings/${id}`;
    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        'Authorization': `Bearer ${token}`,
      }
    });

    dispatch({type: receiveDeleteListingsType, id})
  },
  update: (id, title, body, itemId) => async (dispatch,getState) => {
    const token = getState().auth.token;
    dispatch({ type: requestCreateListingsType });

    const url = `/api/Listings/${id}`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        listing: {
          itemId: itemId,
          listingTitle: title,
          listingBody: body
        }
      })
    });
    const res = await response.json();


    dispatch({type: receiveCreateListingsType, ...res})
    dispatch(push(`/listings/${id}`));
  },
  create: (title, body, itemId) => async (dispatch,getstate) => {
    const token = getstate().auth.token;
    dispatch({ type: requestUpdateListingsType });

    const url = `/api/Listings/`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        itemId: itemId,
        listingTitle: title,
        listingBody: body,
      })
    });
    const res = await response.json();


    dispatch({type: receiveCreateListingsType, item:res})
    dispatch(push(`/listings/${res.id}`));
  }
};

export const reducer = (state = initialState, action) => {

  if (action.type === requestListingsType) {
    return {
      ...state,
      isLoading: true
    };
  }

  if (action.type === receiveListingsType) {
    return {
      ...state,
      list: action.listings,
      isLoading: false
    };
  }

  if (action.type === receiveDeleteListingsType) {
    return {
      ...state,
      list: state.list.filter((l) => l.id !== action.id),
      isLoading: false
    };
  }

  if (action.type === receiveUpdateListingsType) {
    return {
      ...state,
      list: state.list.map((l) => l.id !== action.id ? l : {}),
      isLoading: false
    };
  }

  if (action.type === receiveCreateListingsType) {
    return {
      ...state,
      list: [...state.list,action.item],
      isLoading: false
    };
  }

  return state;
};
