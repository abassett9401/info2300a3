import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux'
import {push} from 'connected-react-router'
import { Button ,ButtonToolbar ,DropdownButton,MenuItem} from 'react-bootstrap'
import { actionCreators as listingActions } from '../store/listings';
import { actionCreators as itemsActions } from '../store/items';

const EditPost = ({match}) => {
    const {id} = match.params;
    const dispatch = useDispatch();
    const listing = useSelector((s) => s.listings.list.find((l) => l.id.toString() === id));
    const items = useSelector((s) => s.items.list);

    const [title, setTitle] = useState(listing && listing.listingTitle);
    const [body, setBody] = useState(listing && listing.listingBody);
    const [itemId, setItemId] = useState(listing && listing.itemId);

    useEffect(() => {
        if(listing){
            setTitle(listing.listingTitle);
            setBody(listing.listingBody);
            setItemId(listing.itemId);
        }
    },[listing]);

    useEffect(() => {
        dispatch(listingActions.list());
        dispatch(itemsActions.list());
    }, [])
    const item = items.find((i) => i.id === itemId);
    return (
    <div>
        {listing && items ? (
            <React.Fragment>
                    <div className="form-group">
                        <label>Title</label>
                        <input type="username" className="form-control" placeholder="Enter username" value={title} onChange={(e) => setTitle(e.target.value)}/>
                    </div>
                    <div className="form-group">
                        <label>Body</label>
                        <textarea placeholder="Enter Body" class="form-control" value={body} onChange={(e) => setBody(e.target.value)} />
                    </div>
                    <div className="form-group"> 
                        <label>item</label>
                        <ButtonToolbar>
                            <DropdownButton title={item && item.itemName} id="dropdown-size-medium" onSelect={(id) => setItemId(id)}>
                                {items.map((item) => (
                                    <MenuItem eventKey={item.id}>{item.itemName}</MenuItem>
                                ))}
                            </DropdownButton>
                        </ButtonToolbar>
                    </div>
                    <Button bsStyle="primary" bsSize="small" onClick={() => dispatch(listingActions.update(listing.id, title, body, itemId))}>
                        Update
                    </Button>
                    <Button bsStyle="danger" bsSize="small" onClick={() => dispatch(push(`/listings`))}>
                        Cancel
                    </Button>
            </React.Fragment>
        ) : (
        <p>Loading</p>
        )}
   
    </div>
)};

export default EditPost;
