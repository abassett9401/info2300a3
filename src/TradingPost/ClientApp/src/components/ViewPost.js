import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux'
import {push} from 'connected-react-router'
import { Button ,ButtonToolbar ,DropdownButton,MenuItem} from 'react-bootstrap'
import { actionCreators as listingActions } from '../store/listings';
import { actionCreators as itemsActions } from '../store/items';

const ViewPost = ({match}) => {
    const {id} = match.params;
    const dispatch = useDispatch();
    const listing = useSelector((s) => s.listings.list.find((lst) => lst.id.toString() === id));
    const items = useSelector((s) => s.items.list);
    console.log(id.toString());
    useEffect(() => {
        dispatch(listingActions.list());
        dispatch(itemsActions.list());
    }, [])
    
    return (
    <div>
        {listing && items.length > 0 ? (
            <React.Fragment>
                   <h3>{listing.listingTitle}</h3>
                   <ButtonToolbar>
                            <DropdownButton title={items.find((i) => i.id === listing.itemId).itemName} id="dropdown-size-medium" disabled>
                                {items.map((item) => (
                                    <MenuItem eventKey={item.id}>{item.itemName}</MenuItem>
                                ))}
                            </DropdownButton>
                        </ButtonToolbar>
                        <p>{listing.listingBody}</p>
            </React.Fragment>
        ) : (
        <p>Loading...</p>
        )}
   
    </div>
)};

export default ViewPost;
