import React, { useState } from "react";
import {actionCreators} from '../store/auth'
import {useDispatch} from "react-redux"

const Login = () => {
    const dispatch = useDispatch();
    const [userName, setUsername] = useState('');
    const [password, setPassword] = useState('');


    return (
        <React.Fragment>
            <h3>Sign In</h3>

            <div className="form-group">
                <label>Username</label>
                <input type="email" className="form-control" placeholder="Enter username" value={userName} onChange={(e) => setUsername(e.target.value)}/>
            </div>

            <div className="form-group">
                <label>Password</label>
                <input type="password" className="form-control" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)}/>
            </div>
            <button type="button" className="btn btn-primary btn-block" onClick={() => dispatch(actionCreators.login(userName, password))}>Submit</button>
        </React.Fragment>
    );
}

export default Login;