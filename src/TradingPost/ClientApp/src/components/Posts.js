import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux'
import {push} from 'connected-react-router'
import {ListGroup, ListGroupItem, Button, Glyphicon, Row, Col,Grid } from 'react-bootstrap'
import { actionCreators as listingActions } from '../store/listings';
import { actionCreators as itemsActions } from '../store/items';

const ListingItem = ({listingTitle, listingBody, userId,listId}) => {
    const dispatch = useDispatch();
    const currentUserId = useSelector((s) => (s.auth.user && s.auth.user.id ) || null);
    
    return (
    <button type="button" className="list-group-item list-group-item-action" onClick={() => dispatch(push(`/listings/${listId}`))}>
        <Grid fluid>
        <Row>
                <Col sm={10}>
                    <h4>{listingTitle}</h4>
                    <p>{listingBody}</p>
                </Col>
                {userId === currentUserId && 
                    ( <Col sm={2}>
                    <Grid fluid>
                        <Row>
                        <Col sm={6}>
                            <Button bsStyle="danger" bsSize="small" onClick={(e) => {
                                e.stopPropagation();
                                dispatch(listingActions.delete(listId))}}>
                                <Glyphicon glyph="trash" />
                            </Button>
                        </Col>
                        <Col sm={6}>
                            <Button bsStyle="primary" bsSize="small" onClick={(e) => {
                                e.stopPropagation();
                                dispatch(push(`/listings/${listId}/edit`))
                                }}>
                                <Glyphicon glyph="wrench" />
                            </Button>
                        </Col>
                    </Row>
                    </Grid>
                </Col>
                    )
                }
            </Row>
        </Grid>
    </button>
)}

const Posts = props => {
    const dispatch = useDispatch();
    const token = useSelector((s) => s.auth.token);
    const listings = useSelector((s) => s.listings.list);
    const items = useSelector((s) => s.items.list);
    useEffect(() => {
        dispatch(listingActions.list());
        dispatch(itemsActions.list());
    }, [])
    return (
  <div>
    <h1>Listings</h1>
    {token && <Button bsStyle="primary" bsSize="small" onClick={() => dispatch(push(`/listings/create`))}>
        Create
    </Button>}
    {items.map((item) => (
        <React.Fragment>
            <h3>{item.itemName}</h3>
            
            {listings.filter((l) => l.itemId === item.id).map((list) => (
                <ListGroup>
                    <ListingItem listingTitle={list.listingTitle} listingBody={list.listingBody} listId={list.id} userId={list.userId}/>
                </ListGroup>
            ))}
        </React.Fragment>
    ))}
    
    
  </div>
)};

export default Posts;
