﻿import React from 'react';
import {useSelector, useDispatch} from 'react-redux'
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import {actionCreators} from '../store/auth'
import './NavMenu.css';

export default props => {
  const dispatch = useDispatch();
  const token = useSelector((state) => state.auth.token);
  return (
  <Navbar inverse fixedTop fluid collapseOnSelect>
    <Navbar.Header>
      <Navbar.Brand>
        <Link to={'/'}>Trading Post</Link>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav>
        <LinkContainer to={'/'} exact>
          <NavItem>
            <Glyphicon glyph='home' /> Home
          </NavItem>
        </LinkContainer>
        <LinkContainer to={'/listings'}>
          <NavItem>
            <Glyphicon glyph='pencil' /> Listings
          </NavItem>
        </LinkContainer>
        {token 
        ? (<NavItem onClick={() => dispatch(actionCreators.logout())}>
          <Glyphicon glyph='user' /> Logout
        </NavItem>)
        : (
          <React.Fragment>
        <LinkContainer to={'/login'}>
        <NavItem>
          <Glyphicon glyph='user' /> Login
        </NavItem>
      </LinkContainer>
      <LinkContainer to={'/register'}>
        <NavItem>
          <Glyphicon glyph='envelope' /> register
        </NavItem>
      </LinkContainer>
        </React.Fragment>)}
        
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);
  }
