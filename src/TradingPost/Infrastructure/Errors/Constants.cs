namespace TradingPost.Infrastructure.Errors
{
    public static class Constants
    {
        public static string NOT_FOUND = "Not Found";
        public static string IN_USE = "In Use";
        public static string NOT_MATCHING = "Not Matching";
    }
}