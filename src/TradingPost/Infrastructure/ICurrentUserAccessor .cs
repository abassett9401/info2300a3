using System.Threading.Tasks;
using TradingPost.Features.User;

namespace TradingPost.Infrastructure
{
    public interface ICurrentUserAccessor 
    {
        Task<UserDto> GetUser();
        string GetUsername();
        int GetId();
        bool isAuthentiated();
    }
}