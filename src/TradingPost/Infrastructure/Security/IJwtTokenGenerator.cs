namespace TradingPost.Infrastructure.Security
{
    public interface IJwtTokenGenerator
    {
        string Create(Models.User username);
    }
}