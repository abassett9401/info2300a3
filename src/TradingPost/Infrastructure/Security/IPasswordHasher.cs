namespace TradingPost.Infrastructure.Security
{
    public class HashResult {
        public byte[] hash;
        public byte[] salt;
    }

    public interface IPasswordHasher
    {
         byte[] Hash(string password, byte[] salt);

         HashResult Hash(string password);
    }
}