using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace TradingPost.Infrastructure.Security
{
    public class PasswordHasher : IPasswordHasher
    {
        public static int KEY_BYTES = 256 / 8;
        public byte[] Hash(string password, byte[] salt) => 
            KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
        );

        public HashResult Hash(string password)
        {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            return new HashResult(){
                salt = salt,
                hash = this.Hash(password, salt)
            };
        }
    }
}