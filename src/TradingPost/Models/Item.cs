using System.Collections.Generic;

namespace TradingPost.Models
{
    public class Item
    {

        public int id { get; set; }
        public string itemName {get; set;}
        public int GameID {get; set;}
        public int quality {get; set;}
        public string description {get; set;}
        public Game Game { get; set; }
    }

}