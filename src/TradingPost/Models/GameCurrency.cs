using System.Collections.Generic;

namespace TradingPost.Models
{
    public class GameCurrency
    {

        public int id { get; set; }
        public int GameID { get; set; }
        public string nameCurrency { get; set; }
        public bool numericCurrency { get; set; }
        public Game Game { get; set; }

    }

}