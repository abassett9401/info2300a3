using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TradingPost.Models
{
    public class ListingComment
    {
        public int id { get; set; }
        [Required]
        public int ListingId { get; set; }
        [Required]
        public int UserId { get; set; }
        public string commentBody { get; set; }
        public string commentTitle { get; set; }
        public DateTime datePosted { get; set; }

        public Listing Listing { get; set; }
        public User User { get; set;}
    }

}