using Microsoft.EntityFrameworkCore;

namespace TradingPost.Models
{
    public class TradingPostContext : DbContext
    {
        public TradingPostContext(DbContextOptions<TradingPostContext> options)
            : base(options)
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<GameCurrency> GameCurrencies { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Server> Servers { get; set; }
        public DbSet<Listing> Listings { get; set; }
        public DbSet<ListingComment> ListingComments { get; set; }
        public DbSet<ProfileComment> ProfileComments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Game>().HasData(
                new Game() { id = 1, gameTitle = "World of Warcraft"});

            modelBuilder.Entity<Item>().HasData(
                new Item() { id = 1, itemName = "Axe", description = "An Axe", GameID = 1, quality = 1 },
                new Item() { id = 2, itemName = "Bow", description = "a Bow", GameID = 1, quality = 1 },
                new Item() { id = 3, itemName = "Chainmail", description = "Medium protection armor", GameID = 1, quality = 1 },
                new Item() { id = 4, itemName = "Plate Body", description = "Heavy protection armor", GameID = 1, quality = 1 },
                new Item() { id = 5, itemName = "Leather Body", description = "Light protection armor", GameID = 1, quality = 1 }
            );
        }
    }
}