using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TradingPost.Models
{
    public class Listing
    {
        public int id { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int ItemId { get; set; }
        public DateTime datePosted { get; set; }
        public string listingTitle { get; set; }
        public string listingBody { get; set; }

        [JsonIgnore]
        public User User { get; set; }
        [JsonIgnore]
        public Item Item { get; set; }

    }

}