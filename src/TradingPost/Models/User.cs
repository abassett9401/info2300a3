using System.Collections.Generic;
using Newtonsoft.Json;

namespace TradingPost.Models
{
    public class User
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
        [JsonIgnore]
        public byte[] hash { get; set; }
        [JsonIgnore]
        public byte[] salt { get; set; }
    }

}