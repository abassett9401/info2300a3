namespace TradingPost.Models
{
    public class Server
    {
        public int id {get; set;}

        public string serverName {get; set;}

        public Region Region {get; set;}

        public int RegionId {get; set;}
    }
}