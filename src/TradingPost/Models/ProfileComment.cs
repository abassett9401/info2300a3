using System;
using System.Collections.Generic;

namespace TradingPost.Models
{
    public class ProfileComment
    {
        public int id { get; set; }
        public int profileId { get; set; }
        public int rating { get; set; }
        public string body { get; set; }
        public DateTime datePosted  { get; set; }
    }

}