using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Infrastructure;
using TradingPost.Infrastructure.Errors;
using TradingPost.Models;

namespace TradingPost.Features.ListingComment
{
    public class Delete
    {
        public class Query : IRequest<ListingCommentDto>
        {
            public int id { get; set; }
        }

         public class Handler : IRequestHandler<Query, ListingCommentDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            private readonly ICurrentUserAccessor _currentUserAccessor;
            public Handler(TradingPostContext context, IMapper mapper, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _mapper = mapper;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<ListingCommentDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var listingComment = await _context.ListingComments.FindAsync(request.id);
                
                if (!await _context.ListingComments.Where(x => x.id == request.id).AnyAsync(cancellationToken))
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new {ListingComment = Constants.NOT_FOUND});
                }

                if (listingComment.UserId == _currentUserAccessor.GetId())
                {
                    _context.ListingComments.Remove(listingComment);
                    await _context.SaveChangesAsync();

                    return _mapper.Map<Models.ListingComment, ListingCommentDto>(listingComment);
                }
                else 
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new {User = Constants.NOT_MATCHING});
                }
            }
        }
        
    }
}