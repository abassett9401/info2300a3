using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Infrastructure.Errors;
using TradingPost.Models;

namespace TradingPost.Features.ListingComment
{
    public class Get
    {
        public class Query : IRequest<ListingCommentDto>
        {
            public int id { get; set; }
        }

         public class Handler : IRequestHandler<Query, ListingCommentDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            public Handler(TradingPostContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ListingCommentDto> Handle(Query request, CancellationToken cancellationToken)
            {
                if (!await _context.ListingComments.Where(x => x.id == request.id).AnyAsync(cancellationToken))
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new {ListingComment = Constants.NOT_FOUND});
                }

                var listingComment = await _context.ListingComments.FindAsync(request.id);

                return _mapper.Map<Models.ListingComment, ListingCommentDto>(listingComment);
            }
        }
        
    }
}