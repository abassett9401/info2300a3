using System;
using System.Collections.Generic;

namespace TradingPost.Features.ListingComment
{
    public class ListingCommentDto
    {
        public int id { get; set; }
        public int listingId { get; set; }
        public int userId { get; set; }
        public string commentBody { get; set; }
        public string commentTitle { get; set; }
        public DateTime datePosted  { get; set; }
    }
}