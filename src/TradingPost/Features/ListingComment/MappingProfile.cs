using AutoMapper;

namespace TradingPost.Features.ListingComment
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Models.ListingComment, ListingCommentDto>(MemberList.None);
        }
    }
}