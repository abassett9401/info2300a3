using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TradingPost.Infrastructure;
using TradingPost.Infrastructure.Errors;
using TradingPost.Infrastructure.Security;
using TradingPost.Models;

namespace TradingPost.Features.ListingComment
{
    public class Update
    {
        public class ListingCommentData
        {
            public int id { get; set; }
            public string commentBody { get; set; }
            public string commentTitle { get; set; }
            public DateTime datePosted  { get; set; }
        }
        public class ListingCommentDataValidator : AbstractValidator<ListingCommentData>
        {
            public ListingCommentDataValidator()
            {
                RuleFor(x => x.id).NotNull().NotEmpty();
                RuleFor(x => x.commentBody).NotNull().NotEmpty();
                RuleFor(x => x.commentTitle).NotNull().NotEmpty();
                RuleFor(x => x.datePosted).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<ListingCommentDto>
        {
            public ListingCommentData ListingComment { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.ListingComment).NotNull().SetValidator(new ListingCommentDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, ListingCommentDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            private readonly ICurrentUserAccessor _currentUserAccessor;
            public Handler(TradingPostContext context, IMapper mapper, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _mapper = mapper;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<ListingCommentDto> Handle(Command request, CancellationToken cancellationToken)
            {
                if(!await _context.ListingComments.Where(x => x.id == request.ListingComment.id).AnyAsync(cancellationToken))
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new {ListingComment = Constants.NOT_FOUND});
                }

                var listingComment = await _context.ListingComments.FindAsync(request.ListingComment.id);
                listingComment.commentBody = request.ListingComment.commentBody;
                listingComment.commentTitle = request.ListingComment.commentTitle;
                listingComment.datePosted = request.ListingComment.datePosted;

                if (listingComment.UserId == _currentUserAccessor.GetId())
                {
                    // Add Listing to context
                    _context.ListingComments.Update(listingComment);

                    // push changes to database
                    await _context.SaveChangesAsync(cancellationToken);

                    return _mapper.Map<Models.ListingComment, ListingCommentDto>(listingComment);
                }
                else
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new { User = Constants.NOT_MATCHING });
                }
                
            }
        }
    }
}