using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Infrastructure.Errors;
using TradingPost.Models;

namespace TradingPost.Features.ListingComment
{
    public class GetAListingsComments
    {
        public class Query : IRequest<List<ListingCommentDto>>
        {
            public int listingId { get; set; }
        }

         public class Handler : IRequestHandler<Query, List<ListingCommentDto>>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            public Handler(TradingPostContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<ListingCommentDto>> Handle(Query request, CancellationToken cancellationToken)
            {
                if (!await _context.Listings.Where(x => x.id == request.listingId).AnyAsync(cancellationToken))
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new {Listing = Constants.NOT_FOUND});
                }

                List<Models.ListingComment> listingComments = await _context.ListingComments.Where(x => x.ListingId == request.listingId).ToListAsync();

                return _mapper.Map<List<Models.ListingComment>, List<ListingCommentDto>>(listingComments);
            }
        }
        
    }
}