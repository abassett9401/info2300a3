using AutoMapper;

namespace TradingPost.Features.Listing
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Models.Listing, ListingDto>(MemberList.None);
        }
    }
}