using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Models;

namespace TradingPost.Features.Listing
{
    public class GetAll
    {
        public class Query : IRequest<List<ListingDto>>
        {
            public int id { get; set; }
        }


         public class Handler : IRequestHandler<Query, List<ListingDto>>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            public Handler(TradingPostContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            } 

            public async Task<List<ListingDto>> Handle(Query request, CancellationToken cancellationToken)
            {
                var listings = await _context.Listings.ToListAsync();

                return _mapper.Map<List<Models.Listing>, List<ListingDto>>(listings);
            }
        }
        
    }
}