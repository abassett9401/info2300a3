using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TradingPost.Infrastructure;
using TradingPost.Infrastructure.Errors;
using TradingPost.Infrastructure.Security;
using TradingPost.Models;

namespace TradingPost.Features.Listing
{
    public class Create
    {
        public class Command : IRequest<ListingDto>
        {
            public int itemId { get; set; }
            public string listingTitle { get; set; }
            public string listingBody { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.itemId).NotNull().NotEmpty();
                RuleFor(x => x.listingTitle).NotNull().NotEmpty();
                RuleFor(x => x.listingBody).NotNull().NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Command, ListingDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            private readonly ICurrentUserAccessor _currentUserAccessor;
            public Handler(TradingPostContext context, IMapper mapper, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _mapper = mapper;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<ListingDto> Handle(Command request, CancellationToken cancellationToken)
            {

                if (_currentUserAccessor.GetId().Equals(0))
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new {User = Constants.NOT_FOUND});
                }

                var listing = new Models.Listing()
                {
                    UserId = _currentUserAccessor.GetId(),
                    ItemId = request.itemId,
                    datePosted = DateTime.Now,
                    listingTitle = request.listingTitle,
                    listingBody = request.listingBody
                };                

                // Add Listing to context
                _context.Listings.Add(listing);
                // push changes to database
                await _context.SaveChangesAsync(cancellationToken);

                return _mapper.Map<Models.Listing, ListingDto>(listing);
            }
        }
    }
}