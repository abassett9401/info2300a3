using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TradingPost.Infrastructure;
using TradingPost.Infrastructure.Errors;
using TradingPost.Infrastructure.Security;
using TradingPost.Models;

namespace TradingPost.Features.Listing
{
    public class Update
    {
        public class ListingData
        {
            [JsonIgnore]
            public int listingId { get; set; }
            public int itemId { get; set; }
            public string listingTitle { get; set; }
            public string listingBody { get; set; }
        }
        public class ListingDataValidator : AbstractValidator<ListingData>
        {
            public ListingDataValidator()
            {
                RuleFor(x => x.itemId).NotNull().NotEmpty();
                RuleFor(x => x.listingTitle).NotNull().NotEmpty();
                RuleFor(x => x.listingBody).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<ListingDto>
        {
            public ListingData Listing { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Listing).NotNull().SetValidator(new ListingDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, ListingDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            private readonly ICurrentUserAccessor _currentUserAccessor;
            public Handler(TradingPostContext context, IMapper mapper, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _mapper = mapper;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<ListingDto> Handle(Command request, CancellationToken cancellationToken)
            {
                if(!await _context.Listings.Where(x => x.id == request.Listing.listingId).AnyAsync(cancellationToken))
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new {Listing = Constants.NOT_FOUND});
                }

                var listing = await _context.Listings.FindAsync(request.Listing.listingId);
                listing.ItemId = request.Listing.itemId;
                listing.listingTitle = request.Listing.listingTitle;
                listing.listingBody = request.Listing.listingBody;


                if (listing.UserId == _currentUserAccessor.GetId())
                {
                    // Add Listing to context
                    _context.Listings.Update(listing);

                    // push changes to database
                    await _context.SaveChangesAsync(cancellationToken);

                    return _mapper.Map<Models.Listing, ListingDto>(listing);
                }
                else
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new { User = Constants.NOT_MATCHING });
                }
            }
        }
    }
}