using System;
using System.Collections.Generic;

namespace TradingPost.Features.Listing
{
    public class ListingDto
    {
        public int id { get; set; }
        public int userId { get; set; }
        public int itemId { get; set; }
        public DateTime datePosted  { get; set; }
        public string listingTitle  { get; set; }
        public string listingBody { get; set; }
    }
}