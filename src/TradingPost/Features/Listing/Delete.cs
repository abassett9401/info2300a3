using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Infrastructure;
using TradingPost.Infrastructure.Errors;
using TradingPost.Models;

namespace TradingPost.Features.Listing
{
    public class Delete
    {
        public class Query : IRequest<ListingDto>
        {
            public int id { get; set; }
        }

         public class Handler : IRequestHandler<Query, ListingDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            private readonly ICurrentUserAccessor _currentUserAccessor;
            public Handler(TradingPostContext context, IMapper mapper, ICurrentUserAccessor currentUserAccessor)
            {
                _context = context;
                _mapper = mapper;
                _currentUserAccessor = currentUserAccessor;
            }

            public async Task<ListingDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var listing = await _context.Listings.FindAsync(request.id);

                if (!await _context.Listings.Where(x => x.id == request.id).AnyAsync(cancellationToken))
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new {Listing = Constants.NOT_FOUND});
                }

                if (listing.UserId == _currentUserAccessor.GetId())
                {
                    _context.Listings.Remove(listing);
                    await _context.SaveChangesAsync();

                    return _mapper.Map<Models.Listing, ListingDto>(listing);
                }
                else
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new {User = Constants.NOT_MATCHING});
                }
                
            }
        }
        
    }
}