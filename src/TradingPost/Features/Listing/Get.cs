using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Infrastructure.Errors;
using TradingPost.Models;

namespace TradingPost.Features.Listing
{
    public class Get
    {
        public class Query : IRequest<ListingDto>
        {
            public int id { get; set; }
        }

         public class Handler : IRequestHandler<Query, ListingDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            public Handler(TradingPostContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ListingDto> Handle(Query request, CancellationToken cancellationToken)
            {
                
                if (!await _context.Listings.Where(x => x.id == request.id).AnyAsync(cancellationToken))
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new {Listing = Constants.NOT_FOUND});
                }

                var listing = await _context.Listings.FindAsync(request.id);

                return _mapper.Map<Models.Listing, ListingDto>(listing);
            }
        }
        
    }
}