using AutoMapper;

namespace TradingPost.Features.Item
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Models.Item, ItemDto>(MemberList.None);
        }
    }
}