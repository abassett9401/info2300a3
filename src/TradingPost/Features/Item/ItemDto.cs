using System.Collections.Generic;
namespace TradingPost.Features.Item
{
    public class ItemDto
    {
        public int id { get; set; }
        public string itemName {get; set;}
        public int GameID {get; set;}
        public int quality {get; set;}
        public string description {get; set;}
    }

    public class AuthenticationDto
    {
        public string token {get; set;}
    }
}