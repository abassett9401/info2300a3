using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Models;

namespace TradingPost.Features.Item
{
    public class Get
    {
        public class Query : IRequest<ItemDto>
        {
            public int id { get; set; }
        }

         public class Handler : IRequestHandler<Query, ItemDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            public Handler(TradingPostContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ItemDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var item = await _context.Items.FindAsync(request.id);

                return _mapper.Map<Models.Item, ItemDto>(item);
            }
        }
        
    }
}