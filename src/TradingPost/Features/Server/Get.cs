using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Models;

namespace TradingPost.Features.Server
{
    public class Get
    {
        public class Query : IRequest<ServerDto>
        {
            public int id { get; set; }
        }

         public class Handler : IRequestHandler<Query, ServerDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            public Handler(TradingPostContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ServerDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var server = await _context.Servers.FindAsync(request.id);

                return _mapper.Map<Models.Server, ServerDto>(server);
            }
        }
        
    }
}