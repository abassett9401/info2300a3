using AutoMapper;

namespace TradingPost.Features.Server
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Models.Server, ServerDto>(MemberList.None);
        }
    }
}