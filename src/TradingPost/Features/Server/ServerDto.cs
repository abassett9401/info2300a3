using System.Collections.Generic;
namespace TradingPost.Features.Server
{
    public class ServerDto
    {
        public int id {get; set;}

        public string serverName {get; set;}

        public int RegionId {get; set;}

    }

    public class AuthenticationDto
    {
        public string token {get; set;}
    }
}