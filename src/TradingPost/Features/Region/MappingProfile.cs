using AutoMapper;

namespace TradingPost.Features.Region
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Models.Region, RegionDto>(MemberList.None);
        }
    }
}