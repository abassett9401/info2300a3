using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Models;

namespace TradingPost.Features.Region
{
    public class Get
    {
        public class Query : IRequest<RegionDto>
        {
            public int id { get; set; }
        }

         public class Handler : IRequestHandler<Query, RegionDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            public Handler(TradingPostContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<RegionDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var region = await _context.Regions.FindAsync(request.id);

                return _mapper.Map<Models.Region, RegionDto>(region);
            }
        }
        
    }
}