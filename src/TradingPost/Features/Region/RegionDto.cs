using System.Collections.Generic;
namespace TradingPost.Features.Region
{
    public class RegionDto
    {
        public int id {get; set;}

        public string region {get; set;}

    }

    public class AuthenticationDto
    {
        public string token {get; set;}
    }
}