  
using System;
using System.Collections.Generic;

namespace TradingPost.Features.ProfileComments
{
    public class ProfileCommentDto
    {
        public int id { get; set; }
        public int listingId { get; set; }
        public int rating { get; set; }
        public string body { get; set; }
        public DateTime datePosted  { get; set; }
    }
}