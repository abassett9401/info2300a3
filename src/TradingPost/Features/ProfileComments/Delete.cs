using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Models;

namespace TradingPost.Features.ProfileComments
{
    public class Delete
    {
        public class Query : IRequest<ProfileCommentDto>
        {
            public int id { get; set; }
        }

         public class Handler : IRequestHandler<Query, ProfileCommentDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            public Handler(TradingPostContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ProfileCommentDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var profileComment = await _context.ProfileComments.FindAsync(request.id);
                _context.ProfileComments.Remove(profileComment);
                await _context.SaveChangesAsync();

                return _mapper.Map<Models.ProfileComment, ProfileCommentDto>(profileComment);
            }
        }
        
    }
}