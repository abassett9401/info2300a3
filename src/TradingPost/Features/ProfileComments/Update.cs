
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TradingPost.Infrastructure.Security;
using TradingPost.Models;

namespace TradingPost.Features.ProfileComments
{
    public class Update
    {
        public class ProfileCommentData
        {
            public int profileId { get; set; }
            public int rating { get; set; }
            public string body { get; set; }
            public DateTime datePosted { get; set; }
        }
        public class ProfileCommentDataValidator : AbstractValidator<ProfileCommentData>
        {
            public ProfileCommentDataValidator()
            {
                RuleFor(x => x.profileId).NotNull().NotEmpty();
                RuleFor(x => x.rating).NotNull().NotEmpty();
                RuleFor(x => x.body).NotNull().NotEmpty();
                RuleFor(x => x.datePosted).NotNull().NotEmpty();

            }
        }
        public class Command : IRequest<ProfileCommentDto>
        {
            public ProfileCommentData ProfileComment { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.ProfileComment).NotNull().SetValidator(new ProfileCommentDataValidator());
            }
        }
        public class Handler : IRequestHandler<Command, ProfileCommentDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            private readonly ILogger _logger;
            public Handler(TradingPostContext context, IMapper mapper, ILogger<Handler> logger)
            {
                _context = context;
                _mapper = mapper;
                _logger = logger;
            }

            public async Task<ProfileCommentDto> Handle(Command request, CancellationToken cancellationToken)
            {
                var profileComment = new Models.ProfileComment()
                {
                    profileId = request.ProfileComment.profileId,
                    rating = request.ProfileComment.rating,
                    body = request.ProfileComment.body,
                    datePosted = request.ProfileComment.datePosted
                };

                _context.ProfileComments.Update(profileComment);
                await _context.SaveChangesAsync(cancellationToken);
                return _mapper.Map<Models.ProfileComment, ProfileCommentDto>(profileComment);
            }
        }
    }
}