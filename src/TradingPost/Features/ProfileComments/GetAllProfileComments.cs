using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Models;

namespace TradingPost.Features.ProfileComments
{
    public class GetAllProfileComments
    {
        public class Query : IRequest<List<ProfileCommentDto>>
        {
            public int profileId { get; set; }
        }

         public class Handler : IRequestHandler<Query, List<ProfileCommentDto>>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            public Handler(TradingPostContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<ProfileCommentDto>> Handle(Query request, CancellationToken cancellationToken)
            {
                List<Models.ProfileComment> profileComments = await _context.ProfileComments.Where(x => x.profileId == request.profileId).ToListAsync();

                return _mapper.Map<List<Models.ProfileComment>, List<ProfileCommentDto>>(profileComments);
            }
        }
        
    }
}