using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Models;

namespace TradingPost.Features.Game
{
    public class Get
    {
        public class Query : IRequest<GameDto>
        {
            public int id { get; set; }
        }

         public class Handler : IRequestHandler<Query, GameDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            public Handler(TradingPostContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<GameDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var game = await _context.Games.FindAsync(request.id);

                return _mapper.Map<Models.Game, GameDto>(game);
            }
        }
        
    }
}