using AutoMapper;

namespace TradingPost.Features.Game
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Models.Game, GameDto>(MemberList.None);
        }
    }
}