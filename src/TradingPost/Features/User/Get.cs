using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TradingPost.Infrastructure.Errors;
using TradingPost.Models;

namespace TradingPost.Features.User
{
    public class Get
    {
        public class Query : IRequest<UserDto>
        {
            public int id { get; set; }
        }

         public class Handler : IRequestHandler<Query, UserDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            public Handler(TradingPostContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<UserDto> Handle(Query request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.FindAsync(request.id);
                if(user is null){
                    throw new RestException(System.Net.HttpStatusCode.NotFound);
                }
                return _mapper.Map<Models.User, UserDto>(user);
            }
        }
        
    }
}