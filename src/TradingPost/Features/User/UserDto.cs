using System.Collections.Generic;

namespace TradingPost.Features.User
{
    public class UserDto
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
    }

    public class AuthenticatonDto
    {
        public string token { get; set; }
    }
}