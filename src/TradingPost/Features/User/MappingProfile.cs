using AutoMapper;

namespace TradingPost.Features.User
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Models.User, UserDto>(MemberList.None);
        }
    }
}