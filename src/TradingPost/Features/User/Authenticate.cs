using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TradingPost.Infrastructure.Security;
using TradingPost.Models;

namespace TradingPost.Features.User
{
    public class Authenticate
    {
        public class Command : IRequest<AuthenticatonDto>
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Username).NotNull().NotEmpty();
                RuleFor(x => x.Password).NotNull().NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Command, AuthenticatonDto>
        {
            private readonly TradingPostContext _context;
            private readonly ILogger _logger;
            private readonly IPasswordHasher _passwordHasher;
            private readonly IJwtTokenGenerator _jwtTokenGenerator;

            public Handler(TradingPostContext context, ILogger<Handler> logger, IPasswordHasher passwordHasher, IJwtTokenGenerator jwtTokenGenerator)
            {
                _context = context;
                _logger = logger;
                _passwordHasher = passwordHasher;
                _jwtTokenGenerator = jwtTokenGenerator;
            }

            public async Task<AuthenticatonDto> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.Where(x => x.userName == request.Username).SingleOrDefaultAsync(cancellationToken);
                if(!user.hash.SequenceEqual(_passwordHasher.Hash(request.Password, user.salt))){
                    throw new NotImplementedException();
                }
                return new AuthenticatonDto(){
                    token = _jwtTokenGenerator.Create(user)
                };
            }
        }
    }
}