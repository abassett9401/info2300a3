using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TradingPost.Infrastructure.Errors;
using TradingPost.Infrastructure.Security;
using TradingPost.Models;

namespace TradingPost.Features.User
{
    public class Create
    {
        public class UserData
        {
            public string Username { get; set; }

            public string Email { get; set; }

            public string Password { get; set; }
        }
        public class UserDataValidator : AbstractValidator<UserData>
        {
            public UserDataValidator()
            {
                RuleFor(x => x.Username).NotNull().NotEmpty();
                RuleFor(x => x.Email).NotNull().NotEmpty();
                RuleFor(x => x.Password).NotNull().NotEmpty();
            }
        }

        public class Command : IRequest<UserDto>
        {
            public UserData User { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.User).NotNull().SetValidator(new UserDataValidator());
            }
        }

        public class Handler : IRequestHandler<Command, UserDto>
        {
            private readonly TradingPostContext _context;
            private readonly IMapper _mapper;
            private readonly IPasswordHasher _passwordHasher;
            public Handler(TradingPostContext context, IMapper mapper, IPasswordHasher passwordHasher)
            {
                _context = context;
                _mapper = mapper;
                _passwordHasher = passwordHasher;
            }

            public async Task<UserDto> Handle(Command request, CancellationToken cancellationToken)
            { 
                // Check if another user had the same username
                if (await _context.Users.Where(x => x.userName == request.User.Username).AnyAsync(cancellationToken))
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new { UserName = Constants.IN_USE });
                }

                // Check if another user had the same email
                if (await _context.Users.Where(x => x.email == request.User.Email).AnyAsync(cancellationToken))
                {
                    // TODO: add propper error we can throw here
                    throw new RestException((HttpStatusCode.BadRequest), new { Email = Constants.IN_USE });
                }
                
                // TODO: Implement function properly
                var hashResult = _passwordHasher.Hash(request.User.Password);

                var user = new Models.User()
                {
                    email = request.User.Email,
                    userName = request.User.Username,
                    hash = hashResult.hash,
                    salt = hashResult.salt
                };
                // Add user to context
                _context.Users.Add(user);
                // push changes to database
                await _context.SaveChangesAsync(cancellationToken);

                return _mapper.Map<Models.User, UserDto>(user);
            }
        }
    }
}