using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TradingPost.Infrastructure;
using TradingPost.Models;

namespace TradingPost.Features.User
{
    public class Update
    {

        public class Command : IRequest<UserDto>
        {
            public string Username { get; set; }
            public string Id {get; set;}
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Username).NotNull().NotEmpty();
            }

            public class Handler : IRequestHandler<Command, UserDto>
            {
                private readonly TradingPostContext _context;
                private readonly IMapper _mapper;
                private readonly ILogger _logger;
                private readonly ICurrentUserAccessor _currentUserAccessor;

                public Handler(TradingPostContext context, IMapper mapper, ILogger<Handler> logger, ICurrentUserAccessor currentUserAccessor)
                {
                    _context = context;
                    _mapper = mapper;
                    _logger = logger;
                    _currentUserAccessor = currentUserAccessor;
                }

                public async Task<UserDto> Handle(Command request, CancellationToken cancellationToken)
                {
                    // Check if another user had the name username
                    if (await _context.Users.Where(x => x.userName == request.Username).AnyAsync(cancellationToken))
                    {
                        // TODO: add propper error we can throw here
                        throw new System.NotImplementedException();
                    }
                    
                    var user = await _context.Users.FindAsync(request.Id);
                    user.userName = request.Username;
                    _context.Update(user);

                    await _context.SaveChangesAsync(cancellationToken);

                    return _mapper.Map<Models.User, UserDto>(user);
                }
            }
        }
    }
}