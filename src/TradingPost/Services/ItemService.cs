using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TradingPost.Models;

namespace TradingPost.Services
{
    public interface IItemService
    {
        Task<Item> FindById(int id);
        Task<List<Item>> GetAll();
    }

    public class ItemService : IItemService
    {
        readonly TradingPostContext _context;
        public ItemService(TradingPostContext context){
            _context = context;
        }

        public async Task<Item> FindById(int id)
        {
           Item item = await _context.Items.Where(x => x.id == id).FirstOrDefaultAsync();

           return item;
        }

        public async Task<List<Item>> GetAll()
        {
            List<Item> items = await _context.Items.ToListAsync();

            return items;
        }


    }
}