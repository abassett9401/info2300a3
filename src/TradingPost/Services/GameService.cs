using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TradingPost.Models;

namespace TradingPost.Services
{
    public interface IGameService
    {
        Task<Game> FindById(int id);
        Task<List<Game>> GetAll();
    }

    public class GameService : IGameService
    {
        readonly TradingPostContext _context;
        public GameService(TradingPostContext context){
            _context = context;
        }

        public async Task<Game> FindById(int id)
        {
           Game game = await _context.Games.Where(x => x.id == id).FirstOrDefaultAsync();

           return game;
        }

        public async Task<List<Game>> GetAll()
        {
            List<Game> games = await _context.Games.ToListAsync();

            return games;
        }


    }
}