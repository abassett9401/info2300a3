using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TradingPost.Models;
using TradingPost.Services;

namespace TradingPost.Controllers
{
    [Route("api/[controller]")]
    public class ItemController : Controller
    {
        private readonly IItemService _ItemService;
        public ItemController(IItemService itemService)
        {
            _ItemService = itemService;
        }

        [HttpGet]
        public async Task<IActionResult> GetItems() {
            if (ModelState.IsValid)
            {
                var items = await _ItemService.GetAll();

                return StatusCode(200, items);
            }
            return BadRequest();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetItemById([FromRoute] int id) {
            if (ModelState.IsValid)
            {
                var items = await _ItemService.FindById(id);

                return StatusCode(200, items);
            }
            return BadRequest();
        }
    }
}