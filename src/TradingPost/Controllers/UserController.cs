using Microsoft.AspNetCore.Mvc;

using MediatR;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using TradingPost.Features.User;
using Microsoft.AspNetCore.Authorization;
using TradingPost.Infrastructure;
using Swashbuckle.AspNetCore.Annotations;

namespace TradingPost.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class UserController : Controller
    {

        private readonly ILogger _logger;
        readonly IMediator _mediator;
        private readonly ICurrentUserAccessor _currentUserAccessor;
        public UserController(IMediator mediator, ILogger<UsersController> logger, ICurrentUserAccessor currentUserAccessor)
        {
            _mediator = mediator;
            _logger = logger;
            _currentUserAccessor = currentUserAccessor;
        }

        [HttpGet]
        [SwaggerOperation(
            Summary = "Gets the current user",
            Description = "Requires logged in user",
            OperationId = "GetSelf"
)]
        public async Task<UserDto> self()
    {
        return await _currentUserAccessor.GetUser();
    }
}
}