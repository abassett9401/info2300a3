using Microsoft.AspNetCore.Mvc;

using MediatR;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using TradingPost.Features.ListingComment;
using Microsoft.AspNetCore.Authorization;

namespace TradingPost.Controllers
{
    [Route("api/Listings/{listingId}/comments")]
    public class ListingCommentController : Controller
    {

        private readonly ILogger _logger;
        readonly IMediator _mediator;
        public ListingCommentController(IMediator mediator, ILogger<ListingController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ListingCommentDto> Create([FromBody] Create.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet("{id}")]
        public async Task<ListingCommentDto> GetById([FromRoute] int id)
        {
            return await _mediator.Send(new Get.Query() {
                id = id 
            });
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ListingCommentDto> Delete ([FromRoute] int id) 
        {
            return await _mediator.Send(new Delete.Query() {
                id = id
            });
        }

        [Authorize]
        [HttpPost("{id}")]
        public async Task<ListingCommentDto> Update([FromBody] Update.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet]
        public async Task<List<ListingCommentDto>> GetAListingsComments ([FromRoute] int listingId) 
        {            
            return await _mediator.Send(new GetAListingsComments.Query() {
                listingId = listingId
            });
        }
    }
}
