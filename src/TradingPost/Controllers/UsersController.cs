using Microsoft.AspNetCore.Mvc;

using MediatR;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using TradingPost.Features.User;

namespace TradingPost.Controllers
{
    [Route("[controller]")]
    public class UsersController : Controller
    {

        private readonly ILogger _logger;
        readonly IMediator _mediator;
        public UsersController(IMediator mediator, ILogger<UsersController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<UserDto> Create([FromBody] Create.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet("{id}")]
        public async Task<UserDto> GetById([FromRoute] int id)
        {
            return await _mediator.Send(new Get.Query() {
                id = id 
            });
        }

        [HttpPost("login")]
        public async Task<AuthenticatonDto> Login([FromBody] Authenticate.Command command)
        {
            return await _mediator.Send(command);
        }
    }
}