using Microsoft.AspNetCore.Mvc;

using MediatR;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using TradingPost.Features.Listing;
using Microsoft.AspNetCore.Authorization;

namespace TradingPost.Controllers
{
    [Route("api/Listings")]
    public class ListingController : Controller
    {

        private readonly ILogger _logger;
        readonly IMediator _mediator;
        public ListingController(IMediator mediator, ILogger<ListingController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [Authorize]
        [HttpPost]
        public async Task<ListingDto> Create([FromBody] Create.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet]
        public async Task<List<ListingDto>> GetListings()
        {
            return await _mediator.Send(new GetAll.Query());
        }

        [HttpGet("{id}")]
        public async Task<ListingDto> GetById([FromRoute] int id)
        {
            return await _mediator.Send(new Get.Query() {
                id = id 
            });
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ListingDto> Delete ([FromRoute] int id) 
        {
            return await _mediator.Send(new Delete.Query() {
                id = id
            });
        }

        [Authorize]
        [HttpPost("{id}")]
        public async Task<ListingDto> Update([FromRoute] int id,[FromBody] Update.Command command)
        {
            command.Listing.listingId = id;
            return await _mediator.Send(command);
        }
    }
}