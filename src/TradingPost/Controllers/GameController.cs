using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TradingPost.Models;
using TradingPost.Services;

namespace TradingPost.Controllers
{
    [Route("api/[controller]")]
    public class GameController : Controller
    {
        private readonly IGameService _GameService;
        public GameController(IGameService gameService)
        {
            _GameService = gameService;
        }

        [HttpGet]
        public async Task<IActionResult> GetGames() {
            if (ModelState.IsValid)
            {
                var games = await _GameService.GetAll();

                return StatusCode(200, games);
            }
            return BadRequest();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetGameById([FromRoute] int id) {
            if (ModelState.IsValid)
            {
                var games = await _GameService.FindById(id);

                return StatusCode(200, games);
            }
            return BadRequest();
        }
    }
}