﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TradingPost.Migrations
{
    public partial class ItemSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "id", "GameID", "description", "itemName", "quality" },
                values: new object[,]
                {
                    { 1, 1, "An Axe", "Axe", 1 },
                    { 2, 1, "a Bow", "Bow", 1 },
                    { 3, 1, "Medium protection armor", "Chainmail", 1 },
                    { 4, 1, "Heavy protection armor", "Plate Body", 1 },
                    { 5, 1, "Light protection armor", "Leather Body", 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Items",
                keyColumn: "id",
                keyValue: 5);
        }
    }
}
