﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TradingPost.Migrations
{
    public partial class GameSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Games",
                columns: new[] { "id", "gameTitle" },
                values: new object[] { 1, "World of Warcraft" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Games",
                keyColumn: "id",
                keyValue: 1);

        }
    }
}
